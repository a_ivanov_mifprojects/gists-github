import {Component, OnInit} from '@angular/core';
import {LoginListenerService} from '../core/login-listener.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  menu: boolean;
  login: string;
  authorizationLink = 'https://github.com/login/oauth/authorize?client_id=889b8e9037064c724728&scope=gist';

  constructor(private loginListener: LoginListenerService,
              private router: Router) {
  }

  ngOnInit() {
    this.getLogin();
  }

  getLogin() {
    this.loginListener.watch().subscribe(data => {
      this.login = data;
    });
    if (localStorage.getItem('login')) {
      this.login = localStorage.getItem('login');
    }
  }

  showMenu(state: boolean) {
    this.menu = state;
  }

  logOut() {
    this.login = null;
    localStorage.clear();
    this.router.navigate(['main']);
  }
}
