import { TestBed } from '@angular/core/testing';

import { AccessTokenInterceptorService } from './access-token-interceptor.service';

describe('AccessTokenInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccessTokenInterceptorService = TestBed.get(AccessTokenInterceptorService);
    expect(service).toBeTruthy();
  });
});
