import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Token} from '../../interfaces/token';
import {UserInfo} from '../../interfaces/user-info';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  constructor(private http: HttpClient) {
  }

  getToken(code: string) {
    return this.http.get<Token>(`http://localhost:9999/authenticate/${code}`);
  }

  getUserInfo() {
    return this.http.get<UserInfo>('https://api.github.com/user');
  }

}
