import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GistDetails} from '../../interfaces/gist-details';
import {GistForm} from '../../interfaces/gist-form';

@Injectable({
  providedIn: 'root'
})
export class GistService {

  private apiUrl = `https://api.github.com`;

  constructor(private http: HttpClient) {
  }

  getGist(id: number) {
    return this.http.get<GistDetails>(`${this.apiUrl}/gists/${id}`, {
      observe: 'response'
    });
  }

  getGists(path: string, page: string, since: string) {
    return this.http.get<GistDetails[]>(`${this.apiUrl}${path}`, {
      params: {
        page,
        since,
      },
      observe: 'response'
    });
  }

  raw(url: string) {
    return this.http.get(url, {responseType: 'text'});
  }

  newGist(body: GistForm) {
    return this.http.post(`${this.apiUrl}/gists`, body);
  }

  editGist(id: string, body: GistForm) {
    return this.http.patch(`${this.apiUrl}/gists/${id}`, body);
  }

  deleteGist(id: string) {
    return this.http.delete(`${this.apiUrl}/gists/${id}`);
  }
}
