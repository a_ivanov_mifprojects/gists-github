import { TestBed } from '@angular/core/testing';

import { GistStarService } from './gist-star.service';

describe('GistStarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GistStarService = TestBed.get(GistStarService);
    expect(service).toBeTruthy();
  });
});
