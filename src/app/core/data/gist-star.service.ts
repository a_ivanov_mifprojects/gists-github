import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GistStarService {

  private apiUrl = `https://api.github.com`;

  constructor(private http: HttpClient) {
  }

  starGist(id: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Length': '0',
      })
    };
    return this.http.put(`${this.apiUrl}/gists/${id}/star`, httpOptions);
  }

  unStarGist(id: string) {
    return this.http.delete(`${this.apiUrl}/gists/${id}/star`);
  }

  checkStarGist(id: string) {
    return this.http.get(`${this.apiUrl}/gists/${id}/star`);
  }
}
