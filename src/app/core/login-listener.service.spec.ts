import { TestBed } from '@angular/core/testing';

import { LoginListenerService } from './login-listener.service';

describe('LoginListenerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoginListenerService = TestBed.get(LoginListenerService);
    expect(service).toBeTruthy();
  });
});
