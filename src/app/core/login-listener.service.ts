import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginListenerService {

  login = new Subject<string>();

  constructor() {
  }

  watch() {
    return this.login.asObservable();
  }
}
