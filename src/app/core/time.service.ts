import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TimeService {

  constructor() {
  }

  static calculate(createdTime: string, updatedTime: string) {

    let result = '';
    let fileTime;
    if (createdTime === updatedTime
      || new Date(updatedTime).getTime() / 1000 - new Date(createdTime).getTime() / 1000 < 3) {
      result += `Created`;
      fileTime = createdTime;
    } else {
      result += `Updated`;
      fileTime = updatedTime;
    }

    const today = new Date().getTime();
    fileTime = new Date(fileTime).getTime();

    const timeInterval = (today - fileTime) / 1000;

    if (timeInterval < 60) {
      result += ` ${Math.round(timeInterval)} seconds ago`;
      return result;
    } else if (timeInterval < 3600) {
      result += ` ${Math.round(timeInterval / 60)} minutes ago`;
      return result;
    } else {
      result += ` ${Math.round(timeInterval / 3600)} hours ago`;
      return result;
    }
  }
}
