import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {catchError, finalize, map, mergeMap, tap} from 'rxjs/operators';
import {TimeService} from '../time.service';
import {LoaderService} from '../loader.service';
import {GistService} from '../data/gist.service';
import {GistStarService} from '../data/gist-star.service';
import {forkJoin, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GistDetailsResolverService implements Resolve<any> {

  constructor(private gistService: GistService,
              private gistStarService: GistStarService,
              private time: TimeService,
              private router: Router,
              private loaderService: LoaderService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    const id = route.queryParams.id;

    return this.gistService.getGist(id)
      .pipe(
        tap(response => response.body.time = TimeService.calculate(response.body.created_at, response.body.updated_at)),
        mergeMap(response => this.gistStarService.checkStarGist(response.body.id)
          .pipe(
            catchError(() => of(false)),
            map(starred => starred !== false),

            tap(starred => response.body.star = starred),
            map(() => response)
          )),
        map(response => {
          response.body.files = Object.values(response.body.files);

          return response;
        }),
        mergeMap(response => {
          const requests = response.body.files.map(data => {
            return this.gistService.raw(data.raw_url)
              .pipe(
                tap(raw => data.raw = raw),
                map(() => data)
              );
          });

          return forkJoin(requests)
            .pipe(
              tap((files => response.body.files = files)),
              map(() => response)
            );
        }),
        finalize(() => this.loaderService.loader.next(false)),
        catchError(err => {
          this.loaderService.loader.next(false);
          this.router.navigate(['error']);
          return err;
        }));
  }
}
