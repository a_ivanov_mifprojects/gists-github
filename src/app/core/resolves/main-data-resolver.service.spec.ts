import { TestBed } from '@angular/core/testing';

import { MainDataResolverService } from './main-data-resolver.service';

describe('MainDataResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MainDataResolverService = TestBed.get(MainDataResolverService);
    expect(service).toBeTruthy();
  });
});
