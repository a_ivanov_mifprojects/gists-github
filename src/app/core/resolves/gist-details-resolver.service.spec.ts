import { TestBed } from '@angular/core/testing';

import { GistDetailsResolverService } from './gist-details-resolver.service';

describe('GistDetailsResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GistDetailsResolverService = TestBed.get(GistDetailsResolverService);
    expect(service).toBeTruthy();
  });
});
