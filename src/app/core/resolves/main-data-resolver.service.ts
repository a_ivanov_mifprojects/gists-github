import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {TimeService} from '../time.service';
import {catchError, map} from 'rxjs/operators';
import {LoaderService} from '../loader.service';
import {GistService} from '../data/gist.service';
import {GistStarService} from '../data/gist-star.service';

@Injectable({
  providedIn: 'root'
})
export class MainDataResolverService implements Resolve<any> {

  constructor(private gistService: GistService,
              private gistStarService: GistStarService,
              private time: TimeService,
              private loaderService: LoaderService,
              private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    const page = route.queryParams.page || 1;
    const since = route.queryParams.since || '';
    const starredBoolean = /^true$/i.test(route.queryParams.starred);
    let starred: string;
    starredBoolean ? starred = '/starred' : starred = '';

    return this.gistService.getGists('/gists' + starred, page, since)
      .pipe(
        map(response => {
            response.body.map(body => {
              body.time = TimeService.calculate(body.created_at, body.updated_at);
              this.gistStarService.checkStarGist(body.id)
                .subscribe(() => {
                    body.star = true;
                  },
                  () => {
                    body.star = false;
                  });
              body.files = Object.values(body.files);
              this.gistService.raw(body.files[0].raw_url).subscribe(raw => {
                body.files[0].raw = raw;
              });
              return body;
            });
            this.loaderService.loader.next(false);
            return response;
          },
        ),
        catchError(err => {
          this.loaderService.loader.next(false);
          this.router.navigate(['error']);
          return err;
        }));
  }
}
