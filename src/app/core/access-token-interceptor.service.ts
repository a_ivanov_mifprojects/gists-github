import {Injectable} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {LoaderService} from './loader.service';

@Injectable({
  providedIn: 'root'
})
export class AccessTokenInterceptorService implements HttpInterceptor {

  constructor(private loaderService: LoaderService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    this.loaderService.loader.next(true);

    const token = localStorage.getItem('access_token');

    if (token !== null) {
      req = req.clone({
        // headers: new HttpHeaders({
        //   'If-None-Match': ' ',
        // }),
        setParams: {
          'access_token': `${token}`,
        },
      });
    }

    return next.handle(req);
  }
}

