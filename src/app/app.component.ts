import {Component, OnInit} from '@angular/core';
import {LoaderService} from './core/loader.service';
import {NgxUiLoaderService} from 'ngx-ui-loader';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private loader: LoaderService,
              private ngxService: NgxUiLoaderService) {
  }

  ngOnInit() {
    this.isLoader();
  }

  isLoader() {
    this.loader.show().subscribe(state => {
      if (state) {
        this.ngxService.start();
        return;
      }
      this.ngxService.stop();
    });
  }
}
