import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {LoginListenerService} from '../core/login-listener.service';
import {AuthenticateService} from '../core/data/authenticate.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  token: string;
  gitHubCode: string;

  constructor(private route: ActivatedRoute,
              private http: HttpClient,
              private authenticate: AuthenticateService,
              private router: Router,
              private loginListener: LoginListenerService) {
  }

  ngOnInit() {
    this.getGithubCode();
    this.getAccessToken();
  }

  getGithubCode() {
    this.gitHubCode = this.route.snapshot.queryParamMap.get('code');
  }

  getAccessToken() {
    this.authenticate.getToken(this.gitHubCode).subscribe(data => {
      this.token = data.token;
      localStorage.setItem('access_token', data.token);
      this.getUserInfo();
    });
  }

  getUserInfo() {
    this.authenticate.getUserInfo().subscribe(data => {
      localStorage.setItem('login', data.login);
      this.loginListener.login.next(data.login);
      this.router.navigate(['main/gists']);
    });
  }
}
