import {GistFiles} from './gist-files';

export interface GistDetails {
  time: string;
  comments: number;
  comments_url: string;
  commits_url: string;
  created_at: string;
  description: string;
  files: GistFiles[];
  forks_url: string;
  git_pull_url: string;
  git_push_url: string;
  html_url: string;
  id: string;
  node_id: string;
  owner: any;
  public: true;
  truncated: false;
  updated_at: string;
  url: string;
  user: any;
  star: boolean;
}
