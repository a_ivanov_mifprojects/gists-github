export interface GistFiles {
  filename: string;
  language: string;
  raw_url: string;
  raw: string;
  size: number;
  type: string;
  content: string;
}
