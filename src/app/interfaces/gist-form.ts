export interface GistForm {
  description: string;
  filesArray: [{
    filename?: string;
    content?: string;
    name: string;
  }];
  files: {};
  public: boolean;
}
