import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LoaderService} from '../../core/loader.service';
import {GistService} from '../../core/data/gist.service';
import {GistStarService} from '../../core/data/gist-star.service';
import {GistDetails} from '../../interfaces/gist-details';

@Component({
  selector: 'app-gist-details',
  templateUrl: './gist-details.component.html',
  styleUrls: ['./gist-details.component.scss']
})
export class GistDetailsComponent implements OnInit {

  details: GistDetails;
  message: string;

  constructor(private gistService: GistService,
              private gistStarService: GistStarService,
              private route: ActivatedRoute,
              private loader: LoaderService) {
  }

  ngOnInit() {
    this.getDetails();
  }

  getDetails() {
    this.route.data.subscribe(data => {
      this.details = data.details.body;
    });
  }

  deleteGist() {
    this.gistService.deleteGist(this.details.id).subscribe(() => {
        this.message = 'You gist successfully deleted';
        this.loader.loader.next(false);
      },
      () => {
        this.message = 'Have problems';
        this.loader.loader.next(false);
      });
  }

  fileLength(element: number) {
    return element > 1;
  }

  changeStar(element: GistDetails, state: boolean) {
    if (state) {
      this.gistStarService.starGist(element.id)
        .subscribe(() => {
            element.star = true;
            this.loader.loader.next(false);
          },
          () => {
            element.star = false;
            this.loader.loader.next(false);
          });
    }

    this.gistStarService.unStarGist(element.id).subscribe(() => {
        element.star = false;
        this.loader.loader.next(false);
      },
      () => {
        element.star = true;
        this.loader.loader.next(false);
      });
  }
}
