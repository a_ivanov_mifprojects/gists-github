import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GistsComponent} from './gists/gists.component';
import {MainDataResolverService} from '../core/resolves/main-data-resolver.service';
import {NewGistComponent} from './new-gist/new-gist.component';
import {GistDetailsComponent} from './gist-details/gist-details.component';
import {GistDetailsResolverService} from '../core/resolves/gist-details-resolver.service';
import {MainComponent} from './main/main.component';
import {EditGistComponent} from './edit-gist/edit-gist.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: 'gists',
    component: GistsComponent,
    resolve: {
      main: MainDataResolverService
    },
    runGuardsAndResolvers: 'always',
  },
  {
    path: 'details',
    component: GistDetailsComponent,
    resolve: {
      details: GistDetailsResolverService
    }
  },
  {
    path: 'newGist',
    component: NewGistComponent,
  },
  {
    path: 'editGist',
    component: EditGistComponent,
    resolve: {
      details: GistDetailsResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
