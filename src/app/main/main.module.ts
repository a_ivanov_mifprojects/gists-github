import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MainRoutingModule} from './main-routing.module';
import {GistsComponent} from './gists/gists.component';
import {NewGistComponent} from './new-gist/new-gist.component';
import {ReactiveFormsModule} from '@angular/forms';
import {GistDetailsComponent} from './gist-details/gist-details.component';
import {EditGistComponent} from './edit-gist/edit-gist.component';
import {MainComponent} from './main/main.component';
import {AceEditorModule} from 'ng2-ace-editor';
import { GistFormComponent } from './gist-form/gist-form.component';

@NgModule({
  declarations: [
    GistsComponent,
    NewGistComponent,
    GistDetailsComponent,
    EditGistComponent,
    MainComponent,
    GistFormComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    ReactiveFormsModule,
    AceEditorModule
  ]
})
export class MainModule {
}
