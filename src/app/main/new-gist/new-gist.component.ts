import {Component} from '@angular/core';
import {LoaderService} from '../../core/loader.service';
import {GistService} from '../../core/data/gist.service';
import {GistForm} from '../../interfaces/gist-form';

@Component({
  selector: 'app-new-gist',
  templateUrl: './new-gist.component.html',
  styleUrls: ['./new-gist.component.scss']
})
export class NewGistComponent {

  message: string;

  constructor(private gistService: GistService,
              private loaderService: LoaderService) {
  }

  onSubmit(formData: GistForm) {
    this.gistService.newGist(this.setObj(formData)).subscribe(() => {
        this.message = 'You gist successfully created';
        this.loaderService.loader.next(false);
      },
      () => {
        this.message = 'Have problems';
        this.loaderService.loader.next(false);
      });
  }

  setObj(obj: GistForm) {
    const newFiles = {};

    obj.filesArray.map((element) => {
      newFiles[element.name] = {content: element.content};
    });

    obj.files = newFiles;

    delete obj.filesArray;
    return obj;
  }
}
