import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {LoaderService} from '../../core/loader.service';
import {GistService} from '../../core/data/gist.service';
import {GistStarService} from '../../core/data/gist-star.service';
import {GistDetails} from '../../interfaces/gist-details';

@Component({
  selector: 'app-gists',
  templateUrl: './gists.component.html',
  styleUrls: ['./gists.component.scss']
})
export class GistsComponent implements OnInit {

  login: string;
  mainData: GistDetails[];
  pages = {
    start: 1,
    current: 0,
    next: 0
  };
  pagination: boolean;
  paginationButtonState = {
    prev: false,
    next: false
  };

  query = {
    sinceInput: '',
    since: '',
    starred: false
  };

  showFilters = false;
  filterForm: FormGroup;

  showElements = false;

  constructor(private gistService: GistService,
              private gistStarService: GistStarService,
              private route: ActivatedRoute,
              private router: Router,
              private formBuilder: FormBuilder,
              private loader: LoaderService) {
  }

  ngOnInit() {
    this.getMainData();
  }

  getMainData() {
    this.route.data.subscribe(data => {
      if (data.main.body.length === 0) {
        this.showElements = false;
        this.setFilterForm();
        return;
      }
      this.setMainData(data.main);
    });
  }

  setMainData(data: any) {
    this.showElements = true;
    this.mainData = data.body;
    const page = this.route.snapshot.queryParams.page || 1;
    this.pages.current = parseInt(page, 0);
    this.setPagination(data.headers.get('Link'));
    this.login = localStorage.getItem('login');
    this.setFilterForm();
  }

  setSubmitForm() {
    this.filterForm = this.formBuilder.group(
      {
        starred: this.query.starred,
        dateTime: this.query.sinceInput,
      }
    );
  }

  fileLength(element: number) {
    return element > 1;
  }

  setPagination(link: string) {
    if (!link) {
      this.showPagination(false);
      return;
    }

    let pagesNum;
    pagesNum = link.match(/page=[0-9]+/g)
      .map(val => val.replace(/page=/, ''));

    if (parseInt(pagesNum[1], 0) === 1) {
      this.pages.next = this.pages.current;
    } else {
      this.pages.next = parseInt(pagesNum, 0);
    }
    this.showPagination(true);
  }

  showPagination(state: boolean) {
    this.paginationButtons();
    this.pagination = state;
  }

  paginationButtons() {
    this.paginationButtonState.next = true;
    this.paginationButtonState.prev = true;

    if (this.pages.start === this.pages.current) {
      this.paginationButtonState.prev = false;
    } else if (this.pages.current === this.pages.next) {
      this.paginationButtonState.next = false;
    }
  }

  setFilterForm() {
    this.setFilterDateTime();
    this.setFilterStarred();
    this.setSubmitForm();
  }

  submitFilter(formData: FormGroup) {
    this.showFilterMenu(false);
    this.query.starred = formData.value.starred || false;
    this.pages.current = 1;
    if (formData.value.dateTime) {
      this.query.since = new Date(formData.value.dateTime).toISOString();
    } else {
      this.query.since = '';
    }
    this.navigate(0);
  }

  navigate(state?: number) {
    this.pages.current += state;
    this.router.navigate([], {
      queryParams: {
        page: this.pages.current,
        since: this.query.since,
        starred: this.query.starred
      }
    });
  }

  showFilterMenu(state: boolean) {
    if (this.showFilters === state) {
      this.showFilters = !state;
      return;
    }
    this.showFilters = state;

  }

  setFilterStarred() {
    this.query.starred = /^true$/i.test(this.route.snapshot.queryParams.starred);
  }

  setFilterDateTime() {
    this.query.since = this.route.snapshot.queryParams.since;
    if (this.query.since) {
      const dateTime = new Date(this.route.snapshot.queryParams.since);
      const month = ('0' + dateTime.getMonth() + 1).slice(-2);
      const date = ('0' + dateTime.getDate()).slice(-2);
      const year = dateTime.getFullYear();
      this.query.sinceInput = `${year}-${month}-${date}`;
      return;
    }
    this.query.sinceInput = '';
  }

  cleanFilter() {
    this.showFilterMenu(false);
    this.router.navigate(['main/gists']);
  }

  changeStar(element: GistDetails, state: boolean) {
    if (state) {
      this.gistStarService.starGist(element.id)
        .subscribe(() => {
            element.star = true;
            this.loader.loader.next(false);
          },
          () => {
            element.star = false;
            this.loader.loader.next(false);
          });
      return;
    }

    this.gistStarService.unStarGist(element.id).subscribe(() => {
        element.star = false;
        this.loader.loader.next(false);
      },
      () => {
        element.star = true;
        this.loader.loader.next(false);
      });
  }
}
