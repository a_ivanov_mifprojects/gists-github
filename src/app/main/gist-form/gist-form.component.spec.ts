import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GistFormComponent } from './gist-form.component';

describe('GistFormComponent', () => {
  let component: GistFormComponent;
  let fixture: ComponentFixture<GistFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GistFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GistFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
