import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GistDetails} from '../../interfaces/gist-details';

@Component({
  selector: 'app-gist-form',
  templateUrl: './gist-form.component.html',
  styleUrls: ['./gist-form.component.scss']
})
export class GistFormComponent implements OnInit {
  @Input() gistFiles: GistDetails;
  @Output() response = new EventEmitter<FormData>();

  formData: FormGroup;
  showDeleteButton = false;

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.makeForm();
    this.modifyForm();
  }

  onSubmit(form: FormGroup) {
    this.response.emit(form.value);
  }

  setPublic(value: boolean) {
    this.formData.get('public').setValue(value);
  }

  get formFilesArray() {
    return this.formData.get('filesArray') as FormArray;
  }

  makeForm() {
    let description = '';
    if (this.gistFiles) {
      description = this.gistFiles.description;
    }
    this.formData = this.formBuilder.group(
      {
        description: [description, [Validators.required, Validators.minLength(1)]],
        filesArray: this.formBuilder.array([]),
        public: ''
      }
    );
  }

  modifyForm() {
    if (!this.gistFiles) {
      this.addFormFile('', '');
      return;
    }
    this.gistFiles.files.map((file) => {
      this.addFormFile(file.filename, file.raw);
    });
  }

  formFiles(name?: string, raw?: string) {
    return this.formBuilder.group({
      name: [name || '', [Validators.required, Validators.minLength(1)]],
      content: [raw || '', [Validators.required, Validators.minLength(1)]]
    });
  }

  addFormFile(name?: string, raw?: string) {
    const files = this.formData.get('filesArray') as FormArray;
    files.push(this.formFiles(name, raw));
    this.showDelete();
  }

  deleteFile(index: number) {
    const control = <FormArray>this.formData.controls['filesArray'];
    control.removeAt(index);
    this.showDelete();
  }

  showDelete() {
    if (Object.keys(this.formData.get('filesArray').value).length > 1) {
      this.showDeleteButton = true;
      return;
    }
    this.showDeleteButton = false;
  }
}
