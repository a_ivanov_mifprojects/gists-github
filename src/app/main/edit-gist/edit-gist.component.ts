import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {LoaderService} from '../../core/loader.service';
import {GistService} from '../../core/data/gist.service';
import {GistDetails} from '../../interfaces/gist-details';
import {GistForm} from '../../interfaces/gist-form';

@Component({
  selector: 'app-edit-gist',
  templateUrl: './edit-gist.component.html',
  styleUrls: ['./edit-gist.component.scss']
})
export class EditGistComponent implements OnInit {

  files: FormArray;
  details: GistDetails;
  message: string;

  constructor(private formBuilder: FormBuilder,
              private gistService: GistService,
              private route: ActivatedRoute,
              private router: Router,
              private loaderService: LoaderService) {
  }

  ngOnInit() {
    this.getDetails();
  }

  getDetails() {
    this.route.data.subscribe(data => {
        this.details = data.details.body;
      },
      () => {
        this.router.navigate(['error']);
      });
  }

  onSubmit(formData: GistForm) {

    formData.files = this.setObj(formData);
    const id = this.route.snapshot.queryParams.id;

    this.gistService.editGist(id, formData).subscribe(() => {
        this.message = 'You gist successfully edited';
        this.loaderService.loader.next(false);
      },
      () => {
        this.message = 'Have problems';
        this.loaderService.loader.next(false);
      });
  }

  setObj(obj: GistForm) {
    const newFiles = {};

    this.details.files.map((element, index) => {
      if (!obj.filesArray[index]) {
        obj.filesArray.push({
          name: this.details.files[index].filename,
          filename: null,
        });

      } else if (element.filename === obj.filesArray[index].name) {
        if (element.content === obj.filesArray[index].content) {
          delete obj.filesArray[index];
          return;
        }
        obj.filesArray[index] = {
          name: obj.filesArray[index].name,
          content: obj.filesArray[index].content,
        };

      } else if (element.filename !== obj.filesArray[index].name) {
        obj.filesArray[index] = {
          name: this.details.files[index].filename,
          filename: obj.filesArray[index].name,
          content: obj.filesArray[index].content,
        };
      }
    });

    obj.filesArray.map((element) => {
      if (element.filename) {
        newFiles[element.name] = {
          content: element.content,
          filename: element.filename
        };
        return;
      }

      newFiles[element.name] = {
        content: element.content
      };
    });

    return newFiles;
  }
}
